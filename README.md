# IINI aka Improved INI or 2INI
![N|Solid](https://raw.githubusercontent.com/undbsd/improved-ini/master/media/64logo.png)

This is the .ini format with some syntax improvements.

```
PM> Install-Package IINI -Version 1.1.0
```

```
C:\> dotnet add package IINI --version 1.1.0
```

## Examples
This is a standard ini file, let it be called 'source.ini'
```ini
[Section1]
var1=some text  ;Commentary
var2=yet another text   #Commentary

[Section2]
var1=   ;Empty value
```

And this is a similar document, but it is a iini, let it be called 'file.2ini'
```2ini
[Section1]
{
    var1=some text; /*Commentary*/
    var2=yet another text; /*Commentary*/
};
[Section2] {
    var1=   /*Empty value*/
};
```

## Usage

_C# Code Example:_
```cs
using System;
using System.IO;
using IINI;
using IINI.Types;

namespace Test
{
    class IINITester
    {
        static void Main()
        {
            string source = File.ReadAllText("file.2ini");
            Iini processed = Iini.deserialize(source);
            foreach(Section section in processed.sections) {
                Console.WriteLine("Section '{0}':", section.name);
                foreach(Variable variable in section.vars)
                    Console.WriteLine("  {0} = '{1}'", variable.name, variable.value);
                Console.WriteLine();
            }
        }
    }
}
```
