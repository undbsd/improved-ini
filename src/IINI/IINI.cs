﻿using System.Collections.Generic;
using IINI.Types;
using System.Text.RegularExpressions;

namespace IINI
{
	public class Iini
	{
		public readonly List<Section> sections;
		public Iini()
		{
			sections = new List<Section>();	//Init object
		}
		public Iini(params Section[] content)
		{
			sections = new List<Section>();	//Init object
			foreach(Section entry in content)
				sections.Add(entry);	//Add items
		}
		public Section getSection(string name)
		{
			foreach(Section entry in sections) 
				if(entry.name == name)
					return entry;
			throw new ObjectNotFoundException(string.Format("Cannot find '{0}' section", name));
		}
		public string toINI()
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			foreach(Section sect in this.sections) {
				sb.AppendLine(string.Format("[{0}]", sect.name));
				foreach(Variable varb in sect.vars) {
					sb.AppendLine(string.Format("{0}={1}", varb.name, varb.value));
				}
				sb.AppendLine();
			}
			return sb.ToString();
		}
		
		static List<Types.Variable> _variables = new List<Types.Variable>();
		static List<Types.Section> _sections = new List<Types.Section>();
		static bool checkForDupes(List<Types.Variable> array, string name)
		{
			foreach(Types.Variable item in array)
				if(item.name == name)
					return true;
			return false;
		}
		static bool checkForDupes(List<Types.Section> array, string name)
		{
			foreach(Types.Section item in array)
				if(item.name == name)
					return true;
			return false;
		}
		static void addVar(string text)
		{
			Match variable = Regex.Match(text, @"(^[\s]*([\S]+)[\s]*=([^;]*);(.*))", RegexOptions.Singleline | RegexOptions.Compiled);
			if(variable.Success) {
				string name = variable.Groups[2].Value;
				string value = variable.Groups[3].Value;
				Types.Variable item = new Types.Variable(name, value);
				if(!checkForDupes(_variables, name))
					_variables.Add(item);
				else
					throw new Types.RedefineException("Re-assignment of constants is prohibited.");
				if(!string.IsNullOrEmpty(variable.Groups[4].Value)) {
					addVar(variable.Groups[4].Value);
				}
			}
		}
		static void addSec(string text)
		{
			Match section = Regex.Match(text, @"(^[\s]*\[([^\[\]]+)\][\s]*{([^}{]*)}[\s]*;(.*))", RegexOptions.Singleline | RegexOptions.Compiled);
			if(section.Success) {
				string name = section.Groups[2].Value;
				string value = section.Groups[3].Value;
				addVar(value);
				Types.Section output = new Types.Section(name, _variables.ToArray());
				if(!checkForDupes(_sections, name))
					_sections.Add(output);
				else
					throw new Types.RedefineException("Re-assignment of sections is prohibited.");
				_variables.Clear();
				if(!string.IsNullOrEmpty(section.Groups[4].Value)) {
					addSec(section.Groups[4].Value);
				}
			}
		}
		public static Iini deserialize(string input)
		{
			string temp = Regex.Replace(input, @"/\*.*\*/", "");
			addSec(temp);
            Iini output = new Iini(_sections.ToArray());
			_sections.Clear();
			return output;
		}
	}
}
