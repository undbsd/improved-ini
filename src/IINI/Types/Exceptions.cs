﻿using System;

namespace IINI.Types
{
	public class ObjectNotFoundException : Exception
	{
		public ObjectNotFoundException() { }
	 	public ObjectNotFoundException(string message) : base(message) { }
		public ObjectNotFoundException(string message, Exception innerException) : base(message, innerException) { }
	}
	public class RedefineException : Exception
	{
		public RedefineException() { }
	 	public RedefineException(string message) : base(message) { }
		public RedefineException(string message, Exception innerException) : base(message, innerException) { }
	}
}