﻿using System.Collections.Generic;

namespace IINI.Types
{
	public class Section
	{
		public readonly string name;
		public readonly List<Variable> vars;
		public Section(string name)
		{
			this.name = name;
			this.vars = new List<Variable>();
		}
		public Section(string name, params Variable[] content)
		{
			this.name = name;
			this.vars = new List<Variable>();
			foreach(Variable entry in content)
				this.vars.Add(entry);
		}
		public Variable getVar(string name)
		{
			foreach(Variable entry in vars)
				if(entry.name == name)
					return entry;
			throw new ObjectNotFoundException(string.Format("Cannot find variable with '{0}' name", name));
		}
	}
}
