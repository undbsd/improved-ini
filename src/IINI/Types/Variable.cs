﻿namespace IINI.Types
{
	public class Variable
	{
		public readonly string name;	//Name of variable
		public readonly string value;	//Value of variable
		public Variable(string name, string value = null)	//Class constructor
		{
			this.name = name;
			this.value = value ?? "";	//If value marked as null, set value as empty line
		}
	}
}
